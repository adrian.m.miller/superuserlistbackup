# Superuser List Backup

A simple Magisk Module to backup Magisks Superuser list for later restoration

*This is a module requested by XDA member @christantoan, who quite rightly asked why, if i had backup and restore modules for MagiskHide/Deny Lists, why wasnt there the same functionality for the Superuser list.*

After explaining that id thought of it plenty of times, but due to my overwhelming fondness for laziness, put it in the Much Later basket, i set about trying to make his Xmas Wish come true...

**PLEASE NOTE:**

- We (myself, Osm0sis, ipdev and pndwal) all tried to get this backup and restore functionality (along with backup/restore for the magiskhide/deny list - i have modules for that too - see here: [Related Modules](#related-modules)) added natively to Magisk Manager via Magisk's Github, lets just say it was rejected, so here we are...

- These/There are TWO very discinct modules that work together, one backs up (SuperuserListBackup), and one restores (SuperuserListRestore). You need BOTH modules.


---
  
### How It Works: ###

This module:
    
- Queries the magisk.db policies table
- Grabs all the UID's and formats them into a single line for array iteration
- Queries the package list to match the uid to the package name (pm list packages --uid <uid>) and formats the results
- Writes the package names to the backup file (/storage/emulated/0/SuperUserList.txt)

---

### Module Installation: ###

- Download from **[Releases](https://gitlab.com/adrian.m.miller/superuserlistbackup/-/releases)**  
![](https://gitlab.com/adrian.m.miller/superuserlistbackup/-/badges/release.svg)
- Install the module via Magisk app/Fox Magisk Module Manager/MRepo
- Reboot

---

### Usage: ###

- Install module
- Copy /sdcard/SuperuserList.txt off device for safe keeping
  
The module will create a logfile (/storage/emulated/0/SuperUserListBackup.log) on install, which mirrors the information onscreen. If you have any issues, you'll need to start by looking there, and by opening an issue on this repo's Issues  
    
The only active file in the entire module is /common/install.sh, and it is commented.
   
The module will remain installed, unless removed, after the process completes.

It is safe to leave installed and ignored if you like.

You can of course flash the module again at any time to create a new backup file.

**To restore your backup after a new ROM flash etc, see the [SuperuserListRestore Module Repo](https://gitlab.com/adrian.m.miller/superuserlistrestore) for the partner module to restore your backup**

---

### Changelog: ###

Please see: https://gitlab.com/adrian.m.miller/superuserlistbackup/-/blob/main/changelog.md

---

### Related Modules: ###

MagiskHideDenyBackup: https://gitlab.com/adrian.m.miller/magiskhidedenybackup

MagiskHideDenyRestore: https://gitlab.com/adrian.m.miller/magiskhidedenyrestore

---
